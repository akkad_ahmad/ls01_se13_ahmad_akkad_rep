import java.util.Arrays;
import java.util.Random;
public class Aufgabe6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int length = 10000;
		int[] numbers = new int[length];
		Random rand = new Random();
		        
		// Define range of values
		int min = 1;
		int max = 6;
		// Generate random number for each element in array
		for(int i = 0; i < length; i++){
		    numbers[i] = rand.nextInt(max-min) + min;
		    }
		// Print array of random numbers to console as string
		System.out.println(Arrays.toString(numbers));
	}

}
