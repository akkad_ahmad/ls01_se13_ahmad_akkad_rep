/**
 *
 * Beschreibung
 *
 * @version 1.0 vom 27.10.2020
 * @author 
 */

public class ZusatzAufgaben {
  
  public static void main(String[] args) {
    
    int a = 75, b = 23;
    System.out.println(a);
    System.out.println(b);
    
    System.out.println(a + b);
    
    System.out.println(a - b);
    System.out.println(a * b);
    System.out.println(a / b);
    System.out.println(a % b);
    
    System.out.println(a == b);
    
    System.out.println(a > b);
    System.out.println(a != b);
    System.out.println(a <= b);
    
    System.out.println(a <= 50 && a >= 0);
    System.out.println(b <= 50 && b >= 0);

    

    
  } // end of main1

} // end of class Vergleichsoperatoren